<?php
namespace App\Controller;


use App\Entity\Subscription;
use App\Exception\NotFoundException;
use App\Utils\Env;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionDeleteController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     *
     * @Route(
     *     path="/api/subscriptions/{code}",
     *     methods={"DELETE"},
     *     format="json",
     * )
     */
    public function disableSubscription($code)
    {
        /** @var Subscription $subscription */
        $subscription = $this->entityManager->getRepository(Subscription::class)->findOneByCode($code);
        if(is_null($subscription)){
            throw NotFoundException::withField(Subscription::class, 'code', $code);
        }
        $subscription->setDisabled(true);
        $this->entityManager->persist($subscription);;
        $this->entityManager->flush();
        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }


    /**
     *
     * @Route(
     *     path="/api/generate-code",
     *     methods={"GET"},
     *     format="json",
     * )
     * @deprecated
     */
    public function generateSubscriptionCode(Request $request)
    {
        $init = $request->get('init', false);
        if($init && Env::get(Env::APP_ENV) !== "prod") {
            $subscriptions = $this->entityManager->getRepository(Subscription::class)->findAll();
            /** @var Subscription $sub */
            foreach ($subscriptions as $sub) {
                $sub->setCode('init');
                $this->entityManager->persist($sub);
            }
        } else {
            /** @var Subscription[] $subscription */
            $subscriptions = $this->entityManager->getRepository(Subscription::class)->findByCode("init");

            /** @var Subscription $sub */
            foreach ($subscriptions as $sub) {
                $sub->setCode(Uuid::uuid4()->toString());
                $this->entityManager->persist($sub);
            }
        }
        $this->entityManager->flush();

        return new JsonResponse(["updateSubscriptions" => ["init" => $init, "size" => count($subscriptions)]]);
    }
}