<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HealthController extends AbstractController {

    public function __construct()
    {
    }

    /**
     * @Route(
     *     name="health",
     *     path="/health",
     *     methods={"GET"},
     *     defaults={}
     * )
     */
    public function __invoke()
    {
        $data=["status" => "OK"];
        $data["vars"]["CORS_ALLOW_ORIGIN"]=$_ENV["CORS_ALLOW_ORIGIN"];

        $data["vars"]["APP_ENV"]=$_ENV["APP_ENV"];
        $data["vars"]["PHP_VERSION"]=$_ENV["PHP_VERSION"];
        $data["vars"]["COMPOSER_VERSION"]=$_ENV["COMPOSER_VERSION"];

        $database_url = "";
        if (isset($_ENV["DATABASE_URL"])) {
            $database_url = $_ENV["DATABASE_URL"];
        }
        $data["vars"]["DATABASE_URL"]= preg_replace('/\:.+@/', ':********@', $database_url);

        $mailer_url = "";
        if (isset($_ENV["MAILER_URL"])) {
            $mailer_url = $_ENV["MAILER_URL"];
        }
        $data["vars"]["MAILER_URL"]=preg_replace('/password=.+/', 'password=********', $mailer_url);

        return new JsonResponse($data);
    }
}