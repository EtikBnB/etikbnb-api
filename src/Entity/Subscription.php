<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Utils\UuidTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\SubscriptionDeleteController;

/**
 * @ApiResource(
 *     collectionOperations={"get","post"},
 *     itemOperations={"get"},
 *     attributes={
 *       "normalization_context"={"groups"={"read"}}
 *     },
 * )
 * @ORM\Entity()
 */
class Subscription
{
    const PURPOSE_ASK = 'ask';
    const PURPOSE_OFFER = 'offer';
    const PURPOSE_SUPPORT = 'support';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("read")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", options={"default" : "init"}, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("read")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Email(message="preinscription.email.invalid")
     * @Groups("read")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Choice(choices={Subscription::PURPOSE_ASK, Subscription::PURPOSE_OFFER, Subscription::PURPOSE_SUPPORT}, message="preinscription.purpose.invalid")
     * @Groups("read")
     */
    private $purpose;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default" : false})
     * @Groups("read")
     */
    private $alreadyShared =false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default" : false})
     * @Groups("read")
     */
    private $disabled = false;



    public function __construct()
    {
        $this->code = Uuid::uuid4()->toString();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disable
     */
    public function setDisabled(bool $disabled): void
    {
        $this->disabled = $disabled;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPurpose(): ?string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): self
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAlreadyShared(): bool
    {
        return $this->alreadyShared;
    }

    /**
     * @param bool $alreadyShared
     */
    public function setAlreadyShared(bool $alreadyShared): void
    {
        $this->alreadyShared = $alreadyShared;
    }

}
