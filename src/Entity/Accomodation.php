<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "accomodation" = "App\Entity\Accomodation",
 *     "bedroom" = "App\Entity\Bedroom",
 *     "house" = "App\Entity\House",
 *     "appartment" = "App\Entity\Appartment",
 * })
 */
class Accomodation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=1, options={"unsigned": true})
     */
    private $surface;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address")
     * @ORM\JoinColumn(nullable=false)
     */
    private $address;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, options={"unsigned": true})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Member", inversedBy="accomodations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $member;

    /**
     * @ORM\Column(type="date")
     */
    private $availability_from;

    /**
     * @ORM\Column(type="date")
     */
    private $availability_to;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurface(): ?float
    {
        return $this->surface;
    }

    public function setSurface(float $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getMember(): ?Member
    {
        return $this->member;
    }

    public function setMember(?Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getAvailabilityFrom(): ?\DateTimeInterface
    {
        return $this->availability_from;
    }

    public function setAvailabilityFrom(\DateTimeInterface $availability_from): self
    {
        $this->availability_from = $availability_from;

        return $this;
    }

    public function getAvailabilityTo(): ?\DateTimeInterface
    {
        return $this->availability_to;
    }

    public function setAvailabilityTo(\DateTimeInterface $availability_to): self
    {
        $this->availability_to = $availability_to;

        return $this;
    }
}
