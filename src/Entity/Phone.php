<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"international_prefix","number"})})
 */
class Phone extends Contact
{
    /**
     * @ORM\Column(type="string", length=5, name="international_prefix")
     */
    private $internationalPrefix;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $number;

    public function getInternationalPrefix(): ?string
    {
        return $this->internationalPrefix;
    }

    public function setInternationalPrefix(string $internationalPrefix): self
    {
        $this->internationalPrefix = $internationalPrefix;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }
}
