<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
*  @ORM\Table(
 *     indexes={
 *         @ORM\Index(name="idx_point", columns={"gps_coordinates"}, flags={"spatial"}),
 *     },
 * )
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City", inversedBy="addresses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="geometry", options={"geometry_type"="POINT"}, name="gps_coordinates")
     */
    private $gpsCoordinates;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $additionalInformation;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getGpsCoordinates(): string
    {
        return $this->gpsCoordinates;
    }

    /**
     * @param string $gpsCoordinates
     * @return Address
     */
    public function setGpsCoordinates(string $gpsCoordinates): Address
    {
        $this->gpsCoordinates = $gpsCoordinates;
        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

}
