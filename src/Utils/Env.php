<?php


namespace App\Utils;


class Env
{
    const APP_ENV = "APP_ENV";
    const DATABASE_URL = "DATABASE_URL";
    const MAILER_URL = "MAILER_URL";
    const DEFAULT_MAIL_FROM = "DEFAULT_MAIL_FROM";

    static function get(string $key){
        return $_ENV[$key];
    }
}