<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Invitation;
use App\Entity\Subscription;
use App\Exception\NotFoundException;
use App\Exception\SimpleValidationExceptionBuilder;
use App\Utils\Env;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;


class InvitationDataPersister implements ContextAwareDataPersisterInterface {

    /** @var \Swift_Mailer */
    protected $mailer;
    /**  @var Environment */
    private $engine;
    /**  @var EntityManagerInterface */
    private $entityManager;

    public function __construct(\Swift_Mailer $mailer, Environment $engine, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Invitation  && ($context['collection_operation_name'] ?? null) === 'post';
    }

    /**
     * @inheritDoc
     * @param Invitation $data
     * @param array $context
     * @return Invitation
     * @throws NotFoundException
     * @throws \Throwable
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function persist($data, array $context = [])
    {
        if (count($data->getEmails()) > 10) {
            throw SimpleValidationExceptionBuilder::build("invitation.emails.oversize", "emails", count($data->getEmails()));
        }

        /** @var Subscription $subscription */
        $subscription = $this->entityManager->getRepository(Subscription::class)->findOneByEmail($data->getSender());
        if (is_null($subscription)) {
             throw NotFoundException::withField(Subscription::class, 'email', $data->getSender());
        } else if ($subscription->isAlreadyShared()) {
            throw SimpleValidationExceptionBuilder::build("invitation.email.alreadyShared", "sender", $subscription->getEmail());
        }

        $existedEmails = array_map(
            function(Subscription $sub){
                return $sub->getEmail();
            },
            $this->entityManager->getRepository(Subscription::class)->findByEmail($data->getEmails())
        );
        $emailsToSend = array_filter(
            $data->getEmails(),
            function($email) use ($existedEmails){
                return !in_array($email, $existedEmails);
            }
        );
        $data->setEmails($emailsToSend);

        $senderDetails = "<b>".$data->getSender()."</b>";
        $name = trim($data->getFirstname()." ".$data->getLastname());
        if ($name !== '') {
            $senderDetails = "<b>".$name."</b> (".$data->getSender().")";
        }
        if (count($emailsToSend) > 0) {
            $failedRecipients= [];
            $message = (new \Swift_Message('[Etikbnb] On vous invite a participer a une solution pour l\'hebergement du personnel de santé'))
                ->setFrom(Env::get(Env::DEFAULT_MAIL_FROM))
                ->setTo($emailsToSend)
                ->setBody(
                    $this->engine->render('email/invitation.html.twig', ['sender' => $senderDetails]),
                    'text/html'
                );
            $result = $this->mailer->send($message, $failedRecipients);
            if(!$result){
                throw new \Exception("Fail to send emails to " . join(',', $failedRecipients));
            }
        }
        $subscription->setAlreadyShared(true);
        $this->entityManager->persist($subscription);
        $this->entityManager->flush();
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        // Not implemented due to unavailable option
    }
}