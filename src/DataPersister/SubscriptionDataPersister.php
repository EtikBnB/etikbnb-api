<?php
namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Subscription;
use App\Exception\SimpleValidationExceptionBuilder;
use Doctrine\DBAL\Exception\ConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Twig\Environment;


class SubscriptionDataPersister implements  ContextAwareDataPersisterInterface{

    /** @var \Swift_Mailer */
    protected $mailer;
    /**  @var Environment */
    private $decorated;
    /**  @var EntityManagerInterface */
    private $entityManager;

    public function __construct(\Swift_Mailer $mailer, DataPersisterInterface $decorated, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->decorated = $decorated;
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Subscription;
    }

    /**
     * @inheritDoc
     * @param Subscription $data
     * @param array $context
     * @return Subscription
     * @throws ConstraintViolationException
     */
    public function persist($data, array $context = [])
    {
        /** @var Subscription $subscription */
        $subscription = $this->entityManager->getRepository(Subscription::class)->findOneByEmail($data->getEmail());
        if(!is_null($subscription)){
            if($subscription->isDisabled()){
                $data = $subscription;
                $subscription->setDisabled(false);
            } else {
                throw SimpleValidationExceptionBuilder::build("preinscription.email.alreadyExist", "email", $data->getEmail());
            }
        }
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        return $data;
    }

    /**
     * @inheritDoc
     * @var Subscription $data
     */
    public function remove($data, array $context = [])
    {
        // it's not exposed
    }
}