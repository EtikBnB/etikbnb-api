<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Subscription;
use App\Utils\Env;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

class SubscriptionEventSubscriber implements EventSubscriberInterface
{
    const OPTIONS = [
        Subscription::PURPOSE_OFFER => "Merci de votre solidarité en proposant votre hébergement ! </br></br>Nous revenons vers vous dans quelques jours pour des précisions.",
        Subscription::PURPOSE_ASK => "Nous avons bien reçu votre nécessité d'hébergement ! </br></br>Nous revenons vers vous dans quelques jours, bon courage dans ces moments difficiles.",
        Subscription::PURPOSE_SUPPORT => "Merci de votre soutien, c'est très précieux ! <br/></br>Nous vous enverrons des nouvelles très rapidement."
    ];

    /** @var \Swift_Mailer */
    protected $mailer;
    /**
     * @var Environment
     */
    private $engine;

    public function __construct(\Swift_Mailer $mailer, Environment $engine)
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['sendMail',EventPriorities::POST_WRITE]
        ];
    }

    public function sendMail(ViewEvent $event)
    {
        $subscription = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$subscription instanceof Subscription || Request::METHOD_POST !== $method) {
            return;
        }

        $message = (new \Swift_Message('Merci pour votre inscription'))
            ->setFrom(Env::get(Env::DEFAULT_MAIL_FROM))
            ->setTo($subscription->getEmail())
            ->setBody(
                $this->engine->render('email/confirmation.html.twig', ['option' => $this::OPTIONS[$subscription->getPurpose()]]),
                'text/html'
            );

        $this->mailer->send($message);
    }


}