<?php
namespace App\Exception;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class SimpleValidationExceptionBuilder
{
    public static function build(string $message, string $property, string $propertyValue, $objectProperty = null)
    {
        $cv = new ConstraintViolation($message, $message, [$propertyValue], $objectProperty, $property, $propertyValue);
        $cvl = new ConstraintViolationList([$cv]);
        return new ValidationException($cvl);
    }
}