<?php


namespace App\Exception;


use Throwable;

class NotFoundException extends \Exception
{
    private function __construct($message, Throwable $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }

    public static function withMessage($message, Throwable $previous = null)
    {

        return new NotFoundException($message, $previous);
    }

    public static function withField($entityName, $field = null, $value = null, Throwable $previous = null)
    {
        $className = substr($entityName, strrpos($entityName, '\\')+1);
        return self::withMessage("$className was not found with field $field and value $value", $previous);
    }

    public static function withId($entityName, $id = null, Throwable $previous = null)
    {
        return self::withField($entityName, 'id', $id, $previous);
    }
}