FROM composer

RUN apk --no-cache add postgresql-dev
RUN docker-php-ext-install pdo pdo_pgsql

COPY . /var/etikbnb-api

WORKDIR /var/etikbnb-api

ENTRYPOINT /var/etikbnb-api/entrypoint.sh