# RestFull Service API Platform

## Start dev

```
docker-compose up -d
```

By default api is connecting at postgres container started by docker-compose

You can use sqlite , set in .env :
DATABASE_URL=sqlite:///%kernel.project_dir%/var/data.db

Create sqlite database and schema 
```
docker-compose exec workspace bin/console doctrine:database:create
docker-compose exec workspace bin/console doctrine:schema:create
```

access swagger ui : 
http://localhost:8000/api/

## Troubleshouting

### port 5432

If you have this kind of error message :

```
ERROR: for db  Cannot start service db: driver failed programming external connectivity on endpoint etikbnb-api_db_1 (...): Error starting userland proxy: listen tcp 0.0.0.0:5432: bind: address already in use
```

its probably because you already have a PostGreSQL server running on port 5432 on your host machine. Simply disable it to free port 5432 : 

````
sudo service postgresql stop
````

### port 443

If you have the same type of message but regarding port 443 :

```
ERROR: for workspace  Cannot start service workspace: driver failed programming external connectivity on endpoint etikbnb-api_workspace_1 (...): Error starting userland proxy: listen tcp 0.0.0.0:443: bind: address already in use
```

its probably because you already have a local webserver running and listening on the HTTPS port. You can either shutdown this local service

```
sudo service apache2 stop
```

or change the port used: in `docker-compose.yml` change `- 443:443` to `- 8443:443` so that port 8443 is used instead.

### php version 

If the system is complaining about the PHP version

```
This package requires php ^7.4.3 but your PHP version (7.3.8) does not satisfy that requirement.
```

it could be because the `composer` docker image used to build the project is not up-to-date. Just remove the out-of-date one and let docker fetch the latest one.

```
docker rmi composer
docker-compose up -d
```

