#!/bin/bash

composer install
composer update --lock

bin/console doctrine:schema:update --force

php -v

php -S 0.0.0.0:8000 -t public